import asyncio
import logging

from aiogram import Bot, Dispatcher, F, types
from aiogram.enums import ParseMode

BOT_TOKEN = 'xxx' # вставь токен бота вместо xxx
MESSAGE_WITH_PROMOCODE = 'Ваш промокод:\n\nPROMOCODE' # вставь промокод вместо PROMOCODE

dp = Dispatcher()

@dp.message()
async def echo_handler(message: types.Message) -> None:
    await message.answer(MESSAGE_WITH_PROMOCODE)


async def start_bot() -> None:   
    bot = Bot(BOT_TOKEN, parse_mode=ParseMode.HTML)
    await dp.start_polling(bot)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    asyncio.run(start_bot())
